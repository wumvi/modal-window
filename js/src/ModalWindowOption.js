/**
 * @export
 */
class ModalWindowOption {
    /**
     *
     */
    constructor() {
        /**
         *
         * @type {jQuery}
         * @private
         */
        this.$root = null;

        /**
         *
         * @type {?Function}
         */
        this.onShowCallback = null;

        /**
         *
         * @type {?Function}
         */
        this.onHideCallback = null;
    }

    /**
     * @export
     * @param {!jQuery} $root
     */
    setRoot($root) {
        this.$root = $root;
    }

    /**
     * @export
     * @return {?jQuery}
     */
    getRoot() {
        return this.$root;
    }

    /**
     * @export
     * @param {Function} cb
     */
    setOnShowCallback(cb) {
        this.onShowCallback = cb;
    }

    /**
     * @export
     * @param {Function} cb
     */
    setOnHideCallback(cb) {
        this.onHideCallback = cb;
    }

    /**
     * @export
     */
    callShowCallback() {
        if (this.onShowCallback) {
            this.onShowCallback();
        }
    }

    /**
     * @export
     */
    callHideCallback() {
        if (this.onHideCallback) {
            this.onHideCallback();
        }
    }
}
