/**
 *
 * @const {string}
 * @export
 */
const MODAL_WINDOW_SELECTOR_JS = '.js-dbb-modal-window';


/**
 *
 * @const {string}
 * @export
 */
const CLASS_MODAL_WINDOW_SHOW = 'dbb-modal-window--show';


/**
 * @export
 */
class ModalWindow {

    /**
     * Чёрное окно
     * @param {ModalWindowOption} modalWindowOption Настройка
     */
    constructor(modalWindowOption) {
        /**
         * @type {ModalWindowOption}
         * @private
         */
        this.modalWindowOption = modalWindowOption;

        /**
         *
         * @type {?jQuery}
         * @private
         */
        this.$root = this.modalWindowOption.getRoot();

        /**
         * Можно ли закрыть окно, используется для задержки закрытия окна
         * @type {boolean}
         * @private
         */
        this.isCanClose = true;

        this.init();
    }

    /**
     *
     * @private
     */
    init() {
        this.initEvent();
    }

    /**
     *
     * @private
     */
    initEvent() {
        this.$root.find('.js-dbb-close-btn').click(this.onCloseBtnClick.bind(this));
        this.$root.click(this.onRootClick.bind(this));
    }

    /**
     * Клик на кнопки закрытия
     *
     * @return {boolean}
     * @private
     */
    onCloseBtnClick() {
        setTimeout(() => {
            this.hide();
        }, 100);

        return false;
    }

    /**
     * @param {!jQuery.Event} event
     * @private
     * @return {boolean}
     */
    onRootClick(event) {
        let elem = event['target'];
        if (jQuery(elem).parents('.js-dbb-content').length !== 0 || jQuery(elem).hasClass('js-dbb-content')) {
            return true;
        }

        if (!this.isCanClose) {
            return true;
        }

        this.hide();
        return false;
    }

    /**
     * @export
     */
    show() {
        this.$root.addClass(CLASS_MODAL_WINDOW_SHOW);
        let bodyHeight = jQuery(window).height() + window.scrollY;
        jQuery('body').css({'height': bodyHeight, 'overflow': 'hidden'});
        this.modalWindowOption.callShowCallback();

        this.isCanClose = false;
        setTimeout(() => {
            this.isCanClose = true;
        }, 800);
    }

    /**
     * @export
     */
    hide() {
        this.$root.removeClass(CLASS_MODAL_WINDOW_SHOW);
        this.modalWindowOption.callHideCallback();
        jQuery('body').css({'height': '', 'overflow': ''});
    }

    /**
     * Selector для получения родителя
     * @return {string} Selector jQuery
     * @export
     */
    static getSelector() {
        return MODAL_WINDOW_SELECTOR_JS + ':first';
    }

    /**
     *
     * @return {ModalWindowOption}
     * @export
     */
    getOption() {
        return this.modalWindowOption;
    }
}
