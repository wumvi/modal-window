'use strict';
/* global require */

let yaml = require('js-yaml');
let fs = require('fs');
let glob = require('glob');
let objectMerge = require('object-merge');


/**
 *
 * @param {number} oldIndex
 * @param {number} newIndex
 * @returns {Array}
 */
Array.prototype.move = function(oldIndex, newIndex) {
    if (newIndex >= this.length) {
        let k = newIndex - this.length;
        while ((k -= 1) + 1) {
            this.push(undefined);
        }
    }

    this.splice(newIndex, 0, this.splice(oldIndex, 1)[0]);
    return this;
};


/**
 *
 * @param {string} path Пуст к Yaml файлам
 * @param {string} name Назване списка файлов
 * @param {Object} rawData Сырые данные первого yaml файла
 * @param {string} jsSrcDir Директория с исходниками JS
 * @constructor
 */
function JsBuild(path, name, rawData, jsSrcDir) {
    /**
     * Тут будут хранится список JS файлов
     * @type {Array.<string>}
     * @private
     */
    this.jsFileList_ = [];

    /**
     * Имя списка файлов
     * @type {string}
     * @private
     */
    this.name_ = name;

    /**
     * Директория, где хранятся исходники JS файлов
     * @type {string}
     * @private
     */
    this.jsSrcDir = jsSrcDir;

    /**
     *
     * @type {Array.<string>}
     * @private
     */
    this.jsExternsList_ = [];

    /**
     * @private
     * @type {string}
     */
    this.buildFilePath = path;

    this.init_(rawData);
}

/**
 * Инциализация объекта
 * @param {Object} rawData Сырые данные из первого yaml файла
 * @private
 */
JsBuild.prototype.init_ = function(rawData) {
    this.parseData_(this.name_, rawData);

    this.jsFileList_ = this.jsFileList_.filter((elem, pos) => {
        return this.jsFileList_.indexOf(elem) === pos;
    });
};

/**
 * Парсим файлы yaml
 *
 * @param {string} name Навание списка
 * @param {Object} rawData Сырые данные из yaml файла
 * @private
 */
JsBuild.prototype.parseData_ = function(name, rawData) {
    if (rawData[name].hasOwnProperty(JsBuild.INCLUDE_KEY)) {
        let includeList = rawData[name][JsBuild.INCLUDE_KEY];
        for (let includeNum in includeList) {
            if (!includeList.hasOwnProperty(includeNum)) {
                continue;
            }

            let includeName = includeList[includeNum];
            if (!rawData.hasOwnProperty(includeName)) {
                throw new Error('Include "' + includeName + '" not found');
            }

            this.parseData_(includeName, rawData);
        }
    }

    if (rawData[name].hasOwnProperty(JsBuild.EXTERNS_KEY)) {
        let jsList = rawData[name][JsBuild.EXTERNS_KEY].map(elem => {
            return this.buildFilePath + this.jsSrcDir + elem;
        });

        this.jsExternsList_ = this.jsExternsList_.concat(jsList);
    }

    if (rawData[name].hasOwnProperty(JsBuild.JS_LIST_KEY)) {
        let jsList = rawData[name][JsBuild.JS_LIST_KEY].map(elem => {
            if (elem.indexOf('@vendor') !== -1) {
                return elem.replace('@vendor', 'vendor');
            } else if (elem.indexOf('@bower') !== -1) {
                return elem.replace('@bower', BOWER_DIR);
            }

            return this.buildFilePath + this.jsSrcDir + elem;
        });

        this.jsFileList_ = this.jsFileList_.concat(jsList);
    }

    if (rawData[name].hasOwnProperty(JsBuild.JS_IMPORT)) {
        /** @let Array */
        let importList = rawData[name][JsBuild.JS_IMPORT];
        importList.map((item, num) => {
            var data = makeJsBuildItemList(item.replace('@bower', BOWER_DIR))
            data.map((newItem) => {
                // this.jsExternsList_ = objectMerge(this.jsExternsList_, newItem.getExternsList());
                this.jsFileList_ = this.jsFileList_.concat(newItem.getAllJsFileList());
            });
        });
    }
};


/**
 * Получаем название списка
 * @return {string} Название списка
 */
JsBuild.prototype.getName = function() {
    return this.name_;
};


/**
 * Получаем список JS файлов
 * @return {Array.<string>} Список JS файлов
 */
JsBuild.prototype.getAllJsFileList = function() {
    for (let num = 0; num < this.jsFileList_.length; num += 1) {
        let match = this.jsFileList_[num].match(/\[(\d+)\]/);
        if (match) {
            let pos = parseInt(match[1]);
            this.jsFileList_[num] = this.jsFileList_[num].replace(/\[(\d+)\]/, '');
            this.jsFileList_ = this.jsFileList_.move(num, match[1]);
        }
    }

    return this.jsFileList_;
};


/**
 *
 * @return {Array.<string>}
 */
JsBuild.prototype.getExternsList = function() {
    return this.jsExternsList_.map(elem => {
        if (elem.indexOf('@vendor') !== -1) {
            return elem.replace('@vendor', 'vendor');
        } else if (elem.indexOf('@bower') !== -1) {
            return elem.replace('@bower', BOWER_DIR);
        }

        return this.buildFilePath + this.jsSrcDir + elem;
    });
};


/**
 * Ключ из yaml Файла. Какие блоки подключаются
 * @const {string}
 */
JsBuild.INCLUDE_KEY = 'include';

/**
 * Import
 * @const {string}
 */
JsBuild.JS_IMPORT = 'import';

/**
 * Ключ из yaml Файла. Какие файлы подключаются
 * @const {string}
 */
JsBuild.JS_LIST_KEY = 'jsFile';

/**
 *
 * @const {string}
 */
JsBuild.EXTERNS_KEY = 'externs';

const BOWER_DIR = 'public/res/bower_package';


/**
 *
 * @param {string} path
 *
 * @return {Array.<JsBuild>}
 */
function makeJsBuildItemList(path) {
    let settingsBuffer = {};
    let settings = {};

    const fileName = 'build.yaml';

    let fileYamlList = glob.sync(path + '/*.yaml', {
        'ignore': path + fileName
    });

    for (let fileNum = 0; fileNum < fileYamlList.length; fileNum += 1) {
        settings = yaml.safeLoad(fs.readFileSync(fileYamlList[fileNum], 'utf8'));
        settingsBuffer = objectMerge(settingsBuffer, settings);
    }

    settings = yaml.safeLoad(fs.readFileSync(path + fileName, 'utf8'));
    if (!settings.src) {
        throw new Error(`tag "src" not found in ${path + fileName}`);
    }

    const jsSrcDir = settings.src;
    delete settings.src;

    settingsBuffer = objectMerge(settingsBuffer, settings);

    let firstBuildListRaw = [];
    for (let name in settings) {
        if (!settings.hasOwnProperty(name)) {
            continue;
        }

        firstBuildListRaw.push(new JsBuild(path, name, settingsBuffer, jsSrcDir));
    }

    return firstBuildListRaw;
}

module.exports = {
    'JsBuild': JsBuild,
    'fabric': {
        'makeJsBuildItemList': makeJsBuildItemList
    }
};
