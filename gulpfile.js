'use strict';
/* global require, Promise */

let yaml = require('js-yaml');
let fs = require('fs');

let gulp = require('gulp');
let sass = require('gulp-sass');
let merge = require('merge-stream');
let process = require('child_process');
let md5File = require('md5-file');

let folderSassSrc = './sass/';
let folderSassBin = './public/res/bin/';
let versionFile = './cache/version.json';
let path = require('path');


/**
 *
 * @param {boolean} isDev
 * @return {merge}
 */
function buildJs(isDev) {
    let argv = require('yargs').argv;
    let jsName = argv['js-name'];

    console.log('Start build js');

    let compilerPackage = require('google-closure-compiler');
    let closureCompiler = compilerPackage.gulp();
    let gulpJsBuild = require('gulp-js-build');

    let jsMinDir = './public/res/bin/';
    let jsSrcDir = 'js/src/';

    let buildItem = gulpJsBuild.fabric.makeJsBuildItemList('./js/build/')[0];

    let fileList = buildItem.getAllJsFileList();
    console.log('JS count ', fileList.length);

    for (let fileNum = 0; fileNum < fileList.length; fileNum += 1) {
        let filename = fileList[fileNum];
        try {
            console.log(filename);
            fs.statSync();
        } catch (err) {
            console.error(filename);
        }
    }

    let params = {
        compilation_level: 'ADVANCED_OPTIMIZATIONS',
        js_output_file: 'main.min.js',
        language_in: 'ECMASCRIPT6',
        language_out: 'ECMASCRIPT5_STRICT',
        warning_level: 'VERBOSE',
        jscomp_warning: 'checkTypes',
        externs: buildItem.getExternsList(),
        generate_exports: null,
        js: 'node_modules/google-closure-library/closure/goog/base.js',
        output_wrapper: '(function() {%output%}).call(window);'
    };

    if (isDev) {
        params.formatting = 'PRETTY_PRINT';
        params.debug = true;
    }

    return gulp.src(buildItem.getAllJsFileList())
        .pipe(closureCompiler(params))
        .pipe(gulp.dest(jsMinDir));
}


/**
 *
 * @param {boolean} isDev
 * @return {merge}
 */
function buildSass(isDev) {
    // let postcss      = require('gulp-postcss');
    // let sourcemaps   = require('gulp-sourcemaps');
    // let autoprefixer = require('autoprefixer');
    let replace = require('gulp-replace');

    let tasks = [];

    let versionData;
    try {
        versionData = JSON.parse(fs.readFileSync(versionFile, 'utf8'));
    } catch (ex) {
        versionData = {};
    }

    versionData.css = (new Date()).getTime();

    let buildProperties = yaml.safeLoad(fs.readFileSync('sass/build.yaml', 'utf8'));
    for (let num in buildProperties.list) {
        if (!buildProperties.list.hasOwnProperty(num)) {
            continue;
        }

        let folderItem = buildProperties.list[num];
        let mainSassFile = folderSassSrc + '/' + folderItem + '/main.scss';

        let sassOptions = {};
        if (!isDev) {
            sassOptions.outputStyle = 'compressed';
        }

        let tempTask = gulp.src(mainSassFile)
            .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
            //.pipe(sourcemaps.init())
            //.pipe(postcss([ autoprefixer({ browsers: ['last 2 versions', 'IE 9'] }) ]))
            //.pipe(sourcemaps.write('.'))
            .pipe(sass({
                includePaths: [
                    './bower_package/breakpoint-sass/stylesheets'
                ]
            }))
            .pipe(sass(sassOptions).on('error', sass.logError))
            // for IE hack  http://keithclark.co.uk/articles/moving-ie-specific-css-into-media-blocks/media-tests/
            .pipe(replace('IEOnly', 'screen\\0'))
            .pipe(gulp.dest(folderSassBin));
        tasks.push(tempTask);
    }

    fs.writeFileSync(versionFile, JSON.stringify(versionData));

    return merge(tasks);
}

/** @see http://calendar.perfplanet.com/2014/mozjpeg-3-0/ */
gulp.task('js-prod', buildJs.bind(null, false));
gulp.task('js-dev', buildJs.bind(null, true));
gulp.task('sass-prod', buildSass.bind(null, false));
gulp.task('sass-dev', buildSass.bind(null, true));


